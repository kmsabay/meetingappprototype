
var attendeesApiUrl = "http://192.168.1.155/MeetingApp.WebAPI/api/attendees?$filter=MeetingId%20eq%201"
//var crudServiceBaseUrl = "http://168.63.169.147:8080/api/meetings/";

var dataSourceAttendees = new kendo.data.DataSource({
            transport: {
                type: "odata",
                read: {
                        url: function() {
                            return attendeesApiUrl;
                        },
                        dataType: "jsonp",
                    },
                update: {
                        type: "PUT",
                        url: attendeesApiUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function(xhr, s) {
                            console.log('before send');
                        },
                        change:function(data) {
                            alert('changed:' + data);
                        },
                    complete: function(e) {
                        var view = app.view();
                        view.loader.hide();
                        app.navigate("#:back");

                        
                            //alert(item);
                        }
                },
                create: {
                        type: "POST",
                        url: attendeesApiUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function(xhr, s) {
                              alert('create');
                            //var ss = "\"MeetingId\":null";
                            //var value = s.data; 
                            //value = value.replace(ss,  "\"MeetingId\":" + @Model.MeetingId); 
                            //s.data = value;
                            
                        }
                    },
                    destroy: {
                        type: "DELETE",
                        url: function(option) {
                            return attendeesApiUrl + option.RowKey;
                        }
                    },
                  parameterMap: function(options, operation) {
                      
                            return kendo.stringify(options);
                        }
                
            },
             schema: {
                    model: {
                        id: "Id",
                        fields: {
                            
                           MeetingId: { type:"number" },
                            FirstName: { type: "string" },
                            LastName: { type: "string" },
                            EmailAddress: { type: "string" },
                            IsAttending: { type: "bool" },
                            PartitionKey: { type: "string" },
                            RowKey: { type: "string" },
                          
                        }
                    },
                    
                },
              change: function(e) {
                var data = this.data();
                //console.log(data); // displays "77"
                  console.log(kendo.stringify(e)); // displays "77"
                  //alert(kendo.stringify(e));
                 //var view = this.view;
                //view.loader.hide();
                //window.kendoMobileApplication.navigate("#:back");
              }
     
        });

  
     function maintAttendeesViewInit(e) {
        //alert('sdsdsd');
        e.view.element.find("#attendees-listview").kendoMobileListView({
            dataSource: dataSourceAttendees,
            template: $("#attendees-listview-template").html()
        })
        .kendoTouch({
            filter: ">li",
            enableSwipe: true,
            touchstart: touchstart1,
            tap: navigate1,
            swipe: swipe1
        });
    }
     


     function navigate1(e) {
        var itemUID = $(e.touch.currentTarget).data("uid");
         
        kendo.mobile.application.navigate("#edit-detailview?uid=" + itemUID);
    }

    function swipe1(e) {
        var button = kendo.fx($(e.touch.currentTarget).find("[data-role=button]"));
        button.expand().duration(200).play();
    }

    function touchstart1(e) {
        var target = $(e.touch.initialTouch),
            listview = $("#attendees-listview").data("kendoMobileListView"),
            model,
            button = $(e.touch.target).find("[data-role=button]:visible");

        if (target.closest("[data-role=button]")[0]) {
            model = dataSourceAttendees.getByUid($(e.touch.target).attr("data-uid"));
            dataSourceAttendees.remove(model);

            //prevent `swipe`
            this.events.cancel();
            e.event.stopPropagation();
        } else if (button[0]) {
            button.hide();

            //prevent `swipe`
            this.events.cancel();
        } else {
            listview.items().find("[data-role=button]:visible").hide();
        }
    }

    function detailShow1(e) {
        var model = dataSourceAttendees.getByUid(e.view.params.uid);

        kendo.bind(e.view.element, model, kendo.mobile.ui);
    }

    function detailInit1(e) {
        
        var view = e.view;
        view.element.find("#done").bind("click", function() {
             dataSourceAttendees.one("change", function() {
                view.loader.hide();
                window.kendoMobileApplication.navigate("#:back");
             
            });
           
            view.loader.show();
            dataSourceAttendees.sync();
            
        });

        view.element.find("#cancel").data("kendoMobileBackButton").bind("click", function(e) {
            e.preventDefault();
            dataSourceAttendees.one("change", function() {
                view.loader.hide();
                window.kendoMobileApplication.navigate("#:back");
             
            });

            view.loader.show();
            dataSourceAttendees.cancelChanges();
        });
    }
