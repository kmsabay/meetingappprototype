(function() {    
    var AgendaViewModel;
        
    AgendaViewModel = kendo.data.ObservableObject.extend({
        agendaItems: [],

        load: function(agendaItems) {
			this.set("agendaItems", agendaItems);
		},
        
    });
      
     $.extend(window, {
		agendaViewModel: new AgendaViewModel(),
	});
    
})(jQuery);

