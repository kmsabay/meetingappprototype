(function() {    
    var OrganiserPerformanceViewModel;
        
    OrganiserPerformanceViewModel = kendo.data.ObservableObject.extend({
        metrics: [],
        
        load: function(metrics) {
            this.set("metrics", metrics);
		},
        
    });
      
     $.extend(window, {
		organiserPerfViewModel: new OrganiserPerformanceViewModel(),
	});
    
})(jQuery);