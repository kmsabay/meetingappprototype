(function(){
   
    var historyData = new HistoryData();
    
    var HistoryViewModel;
    
    HistoryViewModel = kendo.data.ObservableObject.extend({
        
        init: function() {
            kendo.data.ObservableObject.fn.init.apply(this, [this]);
			
		},
        
        load: function() {
            
            this.set("agendaItems", historyData.getAgendaItems());
        },
        
        agendaItems: [],
        
    });
    
    
    $.extend(window, {
		historyViewModel: new HistoryViewModel()
	});
    
})(jQuery);
