(function(){
   
    var ScheduleViewModel;
    
    
    ScheduleViewModel = kendo.data.ObservableObject.extend({
        
		init: function() {
            kendo.data.ObservableObject.fn.init.apply(this, [this]);
			
		},
        
        load: function(currentMeeting, nextMeeting) {
			//alert('load current schedule');
            //alert(currentAppointment.location);
            //alert(nextAppointment.distance);
            
            this.set("frequency", currentMeeting.frequency); 
            this.set("time", currentMeeting.time); 
            this.set("location", currentMeeting.location); 
            this.set("curTimeRemaining", currentMeeting.timeRemaining); 
            this.set("nextTimeRemaining", nextMeeting.timeRemaining); 
            this.set("distance", nextMeeting.distance); 
		},
        
        resetView: function() {
           //alert('call schedule vm resetView method');
        },
        
        frequency: "",
        time: "",
        location: "",
        curTimeRemaining: "",
        nextTimeRemaining: "",
        distance: ""
   	});
    
    
    $.extend(window, {
		scheduleViewModel: new ScheduleViewModel()
	});
    
})(jQuery);
