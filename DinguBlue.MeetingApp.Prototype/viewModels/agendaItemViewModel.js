(function() {    
    var AgendaItemViewModel;
        
    AgendaItemViewModel = kendo.data.ObservableObject.extend({
        agendaItemDetails: [],

        load: function(agendaItemDetails) {
			this.set("agendaItemDetails", agendaItemDetails);
		},        
    });
      
     $.extend(window, {
		agendaItemViewModel: new AgendaItemViewModel(),
	});
    
})(jQuery);