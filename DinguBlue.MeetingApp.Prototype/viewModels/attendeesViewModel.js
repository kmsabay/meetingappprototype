(function() {    
    var AttendeesViewModel;
        
    AttendeesViewModel = kendo.data.ObservableObject.extend({
        attendees: [],
        apologies: [],  // TODO: Might need a separate view/ViewModel for "Apologies"?!?!
                
        loadAttendees: function(attendees) {
			this.set("attendees", attendees);
		},
        
        loadApologies: function(apologies) {
			this.set("apologies", apologies);
		},        
    });
      
     $.extend(window, {
		attendeesViewModel: new AttendeesViewModel(),
	});
    
})(jQuery);
