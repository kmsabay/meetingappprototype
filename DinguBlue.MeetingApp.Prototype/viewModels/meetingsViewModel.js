
//var crudServiceBaseUrl = "http://127.0.0.1:8080/api/meetings/";
var crudServiceBaseUrl = "http://168.63.169.147:8080/api/meetings/";
 var dataSource = new kendo.data.DataSource({
            transport: {
                type: "odata",
                read: {
                        url: function() {
                            return crudServiceBaseUrl;
                        },
                        dataType: "jsonp",
                    },
                update: {
                        type: "PUT",
                        url: crudServiceBaseUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function(xhr, s) {
                            console.log('before send');
                        },
                        change:function(data) {
                            alert('changed:' + data);
                        },
                    complete: function(e) {
                        var view = app.view();
                        view.loader.hide();
                        app.navigate("#:back");

                        
                            //alert(item);
                        }

                    
                },
                create: {
                        type: "POST",
                        url: crudServiceBaseUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function(xhr, s) {
                              alert('create');
                            //var ss = "\"MeetingId\":null";
                            //var value = s.data; 
                            //value = value.replace(ss,  "\"MeetingId\":" + @Model.MeetingId); 
                            //s.data = value;
                            
                        }
                    },
                    destroy: {
                        type: "DELETE",
                        url: function(option) {
                            return crudServiceBaseUrl + option.RowKey;
                        }
                    },
                  parameterMap: function(options, operation) {
                      
                            return kendo.stringify(options);
                        }
                
            },
             schema: {
                    model: {
                        id: "Id",
                        fields: {
                            
                           Organizer: { type:"string", editable: true, nullable: true },
                            Title: { type: "string", validation: { required: true } },
                            Location: { type: "string", editable: true, nullable: true },
                            PartitionKey: { type: "string" },
                            RowKey: { type: "string" },
                            Schedule: { type: "date" },
                        }
                    },
                    
                },
              change: function(e) {
                var data = this.data();
                console.log(data); // displays "77"
                  
                 //var view = this.view;
                //view.loader.hide();
                //window.kendoMobileApplication.navigate("#:back");
              }
     
        });

  
     function meetingViewInit(e) {
        //alert('sdsdsd');
        e.view.element.find("#meeting-listview").kendoMobileListView({
            dataSource: dataSource,
            template: $("#meeting-listview-template").html()
        })
        .kendoTouch({
            filter: ">li",
            enableSwipe: true,
            touchstart: touchstart,
            tap: navigate,
            swipe: swipe
        });
    }
     


     function navigate(e) {
        var itemUID = $(e.touch.currentTarget).data("uid");
         
        kendo.mobile.application.navigate("#edit-detailview?uid=" + itemUID);
    }

    function swipe(e) {
        var button = kendo.fx($(e.touch.currentTarget).find("[data-role=button]"));
        button.expand().duration(200).play();
    }

    function touchstart(e) {
        var target = $(e.touch.initialTouch),
            listview = $("#meeting-listview").data("kendoMobileListView"),
            model,
            button = $(e.touch.target).find("[data-role=button]:visible");

        if (target.closest("[data-role=button]")[0]) {
            model = dataSource.getByUid($(e.touch.target).attr("data-uid"));
            dataSource.remove(model);

            //prevent `swipe`
            this.events.cancel();
            e.event.stopPropagation();
        } else if (button[0]) {
            button.hide();

            //prevent `swipe`
            this.events.cancel();
        } else {
            listview.items().find("[data-role=button]:visible").hide();
        }
    }

    function detailShow(e) {
        var model = dataSource.getByUid(e.view.params.uid);

        kendo.bind(e.view.element, model, kendo.mobile.ui);
    }

    function detailInit(e) {
        
        var view = e.view;
        view.element.find("#done").bind("click", function() {
             dataSource.one("change", function() {
                view.loader.hide();
                window.kendoMobileApplication.navigate("#:back");
             
            });
           
            view.loader.show();
            dataSource.sync();
            
        });

        view.element.find("#cancel").data("kendoMobileBackButton").bind("click", function(e) {
            e.preventDefault();
            dataSource.one("change", function() {
                view.loader.hide();
                window.kendoMobileApplication.navigate("#:back");
             
            });

            view.loader.show();
            dataSource.cancelChanges();
        });
    }
