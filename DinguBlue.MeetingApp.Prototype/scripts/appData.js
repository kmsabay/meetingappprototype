var AppData = function() {
	
    var _agendaItems;
    var _agendaItem1Details;
        
    var _organiserPerformanceMetrics;
    
	_agendaItems = [
		{ title: "1. New Server", description: "New Server item description goes here..." },
        { title: "2. Old Infrastructure", description: "Old Infrastructure item description goes here..." },
        { title: "3. Middle Blah", description: "Middle Blah item description goes here..." },
        { title: "4. Old Blah", description: "Old Blah item description goes here..." },
        { title: "5. New Blah", description: "New Blah item description goes here..." }    
	];
    
    _agendaItem1Details = [
		{ title: "Opt 1: LAN"},
        { title: "Opt 2: Cloud"},
        { title: "Opt 3: Further"},
        { title: "Desired Outcome"}
	];
        
	_organiserPerformanceMetrics = [
		{ criteria: "Allocated time", weight: "75" },
        { criteria: "Actions completed", weight: "50" },
        { criteria: "Decisions not made", weight: "60" },
        { criteria: "Meeting frequency", weight: "90" },
        { criteria: "Perceived value", weight: "80" },
	];
    
    var loadAttendance = function(entity, meetingId) {
        $("#app-status-ul").append('<li>appData.loadAttendance...</li>');
                
        var crudServiceBaseUrlAttendance = "http://dingubluecloud.cloudapp.net/api/" + entity;
        
        var dataSourceAttendees = new kendo.data.DataSource({
            transport: {
                type: "odata",
                read: {
                    url: function() {
                        return crudServiceBaseUrlAttendance + "?$filter=IsAttending eq true and MeetingId eq " + meetingId;
                    }
                }
            },

        });
        
        var dataSourceApologies = new kendo.data.DataSource({
            transport: {
                type: "odata",
                read: {
                    url: function() {
                        return crudServiceBaseUrlAttendance + "?$filter=IsAttending eq false and MeetingId eq " + meetingId;
                    }
                }
            },
        });
                
        dataSourceAttendees.fetch(function(){
            var data = dataSourceAttendees.data();
            //alert("dataSourceAttendees (" + data.length + ") ==>>" + kendo.stringify(data));
            // For now manual copy, field by field
            var attendees = [];            
            for (var i = 0; i < data.length; i++ )
            {
                attendees[i] = { photo: "images/dummy.png", firstName: data[i].FirstName, lastName: data[i].LastName };
            }
                        
            attendeesViewModel.loadAttendees(attendees);
            $('#cntAttendees').html(data.length);
        });    
        
        dataSourceApologies.fetch(function(){
            var data = dataSourceApologies.data();
            //alert("dataSourceApologies (" + data.length + ") ==>>" + kendo.stringify(data));
            
            var apologies = [];            
            for (var i = 0; i < data.length; i++ )
            {
                apologies[i] = { photo: "images/dummy.png", firstName: data[i].FirstName, lastName: data[i].LastName };
            }
                        
            attendeesViewModel.loadApologies(apologies);                       
            $('#cntApologies').html(data.length);
        });            
                
    };

    
	return {        
		getAgendaItems: function() {
			return _agendaItems;
		},
        
        getAgendaItem1Details: function() {
            return _agendaItem1Details;
        },
        
        getOrganiserPerformanceMetrics: function() {
            return _organiserPerformanceMetrics;
        },
        
		loadAttendance: loadAttendance,        
	};
}




var ScheduleData = function() {
    
    
    var _meetings;
    
    _meetings = [
        { id:"1", frequency:"Weekly", date:"July 1", time:"9:30am", location:"Storey Bridge", timeRemaining:"2 hours", distance:"1 km away" },
        { id:"2", frequency:"Weekly", date:"July 2", time:"8:30am", location:"Storey Bridge", timeRemaining:"25 minutes away", distance:"1600 km away" }
    ];
    
    
    
    return {
        getCurrentMeeting: function() {
            
            var currentAppointment = Enumerable.From(_meetings).Single("$.id==1");
            
             return currentAppointment;   
        },
        
        getNextMeeting: function() {
            
            var nextAppointment = Enumerable.From(_meetings).Single("$.id==2");
            
             return nextAppointment;   
        }
    };
}


var HistoryData = function() {
  
    //var _appData = new AppData()
    
    return {
        getAgendaItems: function() {
            
             return appData.getAgendaItems();   
        }
    };
}

