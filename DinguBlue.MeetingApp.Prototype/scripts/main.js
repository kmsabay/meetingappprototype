document.addEventListener("deviceready", onDeviceReady, false);

var appData; // for now made global

//(function() {
function onDeviceReady() {
    $("#app-status-ul").append('<li>deviceready event received</li>');
    
    appData = new AppData();
    var _pushAlert = new PushAlert();
    var _app,_scheduleData = new ScheduleData();
    
    _app = {
        init: function() {
            _pushAlert.initialize(); // Prepares this device for receiving push notifications
            
			// "Agenda" component
            //
            agendaViewModel.load(appData.getAgendaItems());
            agendaItemViewModel.load(appData.getAgendaItem1Details());  // TODO: might need to be encapsulated within the agendaViewModel, as list         
            
			// "Attendees/Apologies" component
            //
            appData.loadAttendance("attendees", "1"); // hardcode??!
            
			// "Organiser Performance" component
            //
            var metrics = appData.getOrganiserPerformanceMetrics();
            organiserPerfViewModel.load(metrics);                        
       
            historyViewModel.load();
            
            
        },
        
        onScheduleViewShow: function () {
            scheduleViewModel.load(_scheduleData.getCurrentMeeting(),_scheduleData.getNextMeeting());
        },
        
    };
    
	_app.init();
    
    $.extend(window, {
        onScheduleViewShow: _app.onScheduleViewShow
    });
    
//}(jQuery));
};    
