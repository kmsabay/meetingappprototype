var pushNotification;
var registeredDevicesTable;

var PushAlert = function() {
        
    var initialize = function() {
        
        // Our application server, Windows Azure Mobile Service 'dingublue-meetingapp', to remember this device
        // Should be done first before the device registration below
        //
        try {
            var client = new WindowsAzure.MobileServiceClient('https://dingublue-meetingapp.azure-mobile.net/', 'rIlamvQxRzzuBAWjALDAhhFFLtSnyh39');
            registeredDevicesTable = client.getTable('RegisteredDevices');
        } catch(e) {
            navigator.notification.alert('Error: ' + e.description);
        } 
        
        // Register this device in push servers (GCM, APN, ...)
        // Note: should this be a new device, we'd have to report this to our mobile service for caching
        //
		try 
		{ 
        	pushNotification = window.plugins.pushNotification;
        	if (device.platform == 'android' || device.platform == 'Android') {
				$("#app-status-ul").append('<li>registering android</li>');
                pushNotification.register(successHandler, errorHandler, {"senderID":"305413484752","ecb":"onNotificationGCM"});		// <== androidProjectNumber goes here
			} else {
				$("#app-status-ul").append('<li>registering iOS</li>');
            	pushNotification.register(tokenHandler, errorHandler, {"badge":"true","sound":"true","alert":"true","ecb":"onNotificationAPN"});	// required!
        	}
        }
		catch(err) 
		{ 
			txt="There was an error on this page.\n\n"; 
			txt+="Error description: " + err.message + "\n\n"; 
			alert(txt); 
		}                    
    };
        
	return {        
		initialize: initialize
	};    
};

// handle APNS notifications for iOS
function onNotificationAPN(e) {
    if (e.alert) {
         $("#app-status-ul").append('<li>push-notification: ' + e.alert + '</li>');
         navigator.notification.alert(e.alert);
    }
        
    if (e.sound) {
        var snd = new Media(e.sound);
        snd.play();
    }
    
    if (e.badge) {
        pushNotification.setApplicationIconBadgeNumber(successHandler, e.badge);
    }
}

// handle GCM notifications for Android
function onNotificationGCM(e) {
    $("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');
    
    switch( e.event )
    {
        case 'registered':
		if ( e.regid.length > 0 )
		{
			$("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>"); //gaf enlarged
			// Your GCM push server needs to know the regID before it can push to this device
			// here is where you might want to send it the regID for later use.
			console.log("regID = " + e.regid);           
            
            registeredDevicesTable.insert({
				registrationId: e.regid,
                platform: device.platform.toLowerCase(),
                name: device.name // model name or product name
             }).then(function() { $("#app-status-ul").append("<li>registeredDevicesTable.insert invoked.</li>"); }, handleError); // gaf TODO: hook the handleError
		}
        break;
        
        case 'message':
        	// if this flag is set, this notification happened while we were in the foreground.
        	// you might want to play a sound to get the user's attention, throw up a dialog, etc.
        	if (e.foreground)
        	{
				$("#app-status-ul").append('<li>--INLINE NOTIFICATION--' + '</li>');

				// if the notification contains a soundname, play it.
				var my_media = new Media("/android_asset/www/"+e.soundname);
				my_media.play();
			}
			else
			{	// otherwise we were launched because the user touched a notification in the notification tray.
				if (e.coldstart)
					$("#app-status-ul").append('<li>--COLDSTART NOTIFICATION--' + '</li>');
				else
				$("#app-status-ul").append('<li>--BACKGROUND NOTIFICATION--' + '</li>');
			}

			$("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
			$("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
                  
            // refresh application data
            alert("Push notification: " + e.payload.Message);  // intentional blocking for now...
                                            
            if (kendo.stringify(e.payload.Entities).toLowerCase().search("attendees") !== (-1)) { // note: table name hardcoded
                appData.loadAttendance("attendees", "1"); // MeetingId hardcoded for now
            }                        
            
        break;
        
        case 'error':
			$("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
        break;
        
        default:
			$("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
        break;
    }
}

function tokenHandler (result) {
    $("#app-status-ul").append('<li>token: '+ result +'</li>');
    // Your iOS push server needs to know the token before it can push to this device
    // here is where you might want to send it the token for later use.
}

function successHandler (result) {
    $("#app-status-ul").append('<li>success:'+ result +'</li>');
}

function errorHandler (error) {
    $("#app-status-ul").append('<li>error:'+ error +'</li>');
}

function handleError(error) {
    var text = error + (error.request ? ' - ' + error.request.status : '');
    $('#errorlog').append($('<li>').text(text));
}
